package com.cujoai.iot.shield.streaming;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.StreamEvent;
import com.cujoai.iot.shield.domain.StreamEventType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.camel.ProducerTemplate;
import org.springframework.stereotype.Component;

@Component
public class StreamEventsSender {

    private final ProducerTemplate producerTemplate;

    public StreamEventsSender(final ProducerTemplate producerTemplate) {
        this.producerTemplate = producerTemplate;
    }

    public void sendEvents(final List<StreamEvent> streamEvents) {
        for (var i = 0; i < streamEvents.size(); i++) {
            final StreamEvent current = streamEvents.get(i);
            final StreamEventType type = current.getType();
            if (i == 0) {
                invokeCamelRoute(current, createHeaders(0, type));
                continue;
            }
            StreamEvent prev = streamEvents.get(i - 1);
            invokeCamelRoute(current, createHeaders(calculateDelay(current, prev), type));
        }
    }

    private void invokeCamelRoute(final StreamEvent current, final Map<String, Object> headers) {
        producerTemplate.sendBodyAndHeaders(Constants.INPUT_ROUTE, current, headers);
    }

    private long calculateDelay(final StreamEvent current, final StreamEvent prev) {
        return current.getTimestamp() - prev.getTimestamp();
    }

    private Map<String, Object> createHeaders(long delay, StreamEventType type) {
        final HashMap<String, Object> headers = new HashMap<>();
        headers.put(Constants.HEADER_DELAY_MILLIS, delay);
        headers.put(Constants.HEADER_EVENT_TYPE, type);
        return headers;
    }
}
