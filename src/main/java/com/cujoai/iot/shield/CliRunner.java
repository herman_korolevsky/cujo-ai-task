package com.cujoai.iot.shield;

import com.cujoai.iot.shield.domain.StreamEvent;
import com.cujoai.iot.shield.parser.InputParser;
import com.cujoai.iot.shield.streaming.StreamEventsSender;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.json.JsonParseException;
import org.springframework.stereotype.Component;

@Component
public class CliRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShieldApp.class);
    private final InputParser parser;
    private final StreamEventsSender eventsSender;
    private final Scanner scanner;

    public CliRunner(InputParser parser, final StreamEventsSender eventsSender) {
        this.parser = parser;
        this.eventsSender = eventsSender;
        this.scanner = new Scanner(System.in);
    }

    @Override
    public void run(final String... args) {
        final List<StreamEvent> streamEvents = tryParseFile(ArrayUtils.isEmpty(args) ?
            listenForFilePathInput() :
            args[0]
        );
        eventsSender.sendEvents(streamEvents);
    }

    private List<StreamEvent> tryParseFile(final String filePath) {
        try {
            return parser.parseJson(FileUtils.readLines(new File(filePath), Charset.defaultCharset()));
        } catch (IOException | JsonParseException e) {
            LOGGER.error("Failed to parse file: {}. Message: {}", filePath, e.getMessage());
            return tryParseFile(listenForFilePathInput());
        }
    }

    private String listenForFilePathInput() {
        LOGGER.info("\nEnter input file path:");
        return scanner.nextLine();
    }
}
