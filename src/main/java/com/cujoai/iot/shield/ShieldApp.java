package com.cujoai.iot.shield;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.Profile;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ShieldApp {

    @Bean
    public ProducerTemplate producerTemplate(CamelContext camelContext) {
        return camelContext.createProducerTemplate();
    }

    @Bean
    public Map<String, Profile> profileStore() {
        return Collections.synchronizedMap(new HashMap<>());
    }

    @Bean
    public Map<String, Integer> statisticsStore() {
        final Map<String, Integer> statisticsStore = Collections.synchronizedMap(new HashMap<>());
        statisticsStore.put(Constants.AMOUNT_OF_PROTECTED_DEVICES, 0);
        statisticsStore.put(Constants.AMOUNT_OF_SUSPECTED_DEVICES, 0);
        statisticsStore.put(Constants.AMOUNT_OF_MISSED_BLOCKS, 0);
        statisticsStore.put(Constants.AMOUNT_OF_INCORRECT_BLOCKS, 0);
        return statisticsStore;
    }

    public static void main(String[] args) {
        SpringApplication.run(ShieldApp.class, args);
    }
}
