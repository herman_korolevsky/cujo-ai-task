package com.cujoai.iot.shield.domain;

import java.util.List;

public class Profile {

    private final PolicyType policyType;
    private final List<String> exceptionsList;

    public Profile(final PolicyType policyType, final List<String> exceptionsList) {
        this.policyType = policyType;
        this.exceptionsList = exceptionsList;
    }

    public PolicyType getPolicyType() {
        return policyType;
    }

    public List<String> getExceptionsList() {
        return exceptionsList;
    }
}
