package com.cujoai.iot.shield.domain;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProfileCreateStreamEvent extends StreamEvent {

    @SerializedName("default")
    private PolicyType defaultPolicy;
    private List<String> whitelist = new ArrayList<>();
    private List<String> blacklist = new ArrayList<>();

    public PolicyType getDefaultPolicy() {
        return defaultPolicy;
    }

    public void setDefaultPolicy(final PolicyType defaultPolicy) {
        this.defaultPolicy = defaultPolicy;
    }

    public List<String> getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(final List<String> whitelist) {
        this.whitelist = whitelist;
    }

    public List<String> getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(final List<String> blacklist) {
        this.blacklist = blacklist;
    }

    @Override
    public StreamEventType getType() {
        return StreamEventType.PROFILE_CREATE;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("defaultPolicy", defaultPolicy)
            .append("whitelist", whitelist)
            .append("blacklist", blacklist)
            .toString();
    }
}
