package com.cujoai.iot.shield.domain;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProfileUpdateStreamEvent extends StreamEvent {

    private List<String> whitelist = new ArrayList<>();
    private List<String> blacklist = new ArrayList<>();

    public List<String> getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(final List<String> whitelist) {
        this.whitelist = whitelist;
    }

    public List<String> getBlacklist() {
        return blacklist;
    }

    public void setBlacklist(final List<String> blacklist) {
        this.blacklist = blacklist;
    }

    @Override
    public StreamEventType getType() {
        return StreamEventType.PROFILE_UPDATE;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("whitelist", whitelist)
            .append("blacklist", blacklist)
            .toString();
    }
}
