package com.cujoai.iot.shield.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class RequestStreamEvent extends StreamEvent {

    private String requestId;
    private String deviceId;
    private String url;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(final String requestId) {
        this.requestId = requestId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    @Override
    public StreamEventType getType() {
        return StreamEventType.REQUEST;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("requestId", requestId)
            .append("deviceId", deviceId)
            .append("url", url)
            .toString();
    }
}
