package com.cujoai.iot.shield.domain;

public enum ResponseAction {
    ALLOWED,
    BLOCKED,
    SET_DEVICE_TO_QUARANTINE
}
