package com.cujoai.iot.shield.domain;

import java.io.Serializable;

public abstract class StreamEvent implements Serializable {

    private long timestamp;
    private String modelName;

    public abstract StreamEventType getType();

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(final String modelName) {
        this.modelName = modelName;
    }
}
