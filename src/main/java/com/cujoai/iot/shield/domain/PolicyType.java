package com.cujoai.iot.shield.domain;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public enum PolicyType implements Serializable {
    @SerializedName("allow")
    ALLOW,
    @SerializedName("block")
    BLOCK
}
