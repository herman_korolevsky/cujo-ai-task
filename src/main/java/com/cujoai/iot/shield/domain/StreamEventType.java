package com.cujoai.iot.shield.domain;

import java.io.Serializable;

public enum StreamEventType implements Serializable {
    PROFILE_CREATE("profile_create"),
    PROFILE_UPDATE("profile_update"),
    REQUEST("request");

    private final String name;

    StreamEventType(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
