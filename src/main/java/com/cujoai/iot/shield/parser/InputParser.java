package com.cujoai.iot.shield.parser;

import com.cujoai.iot.shield.domain.ProfileCreateStreamEvent;
import com.cujoai.iot.shield.domain.ProfileUpdateStreamEvent;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.StreamEvent;
import com.cujoai.iot.shield.domain.StreamEventType;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.boot.json.JsonParseException;
import org.springframework.stereotype.Service;

@Service
public class InputParser {

    private static final String JSON_PROPERTY_TYPE = "type";
    private final Gson gson;

    public InputParser() {
        RuntimeTypeAdapterFactory<StreamEvent> typeAdapterFactory = RuntimeTypeAdapterFactory
            .of(StreamEvent.class, JSON_PROPERTY_TYPE)
            .registerSubtype(RequestStreamEvent.class, StreamEventType.REQUEST.getName())
            .registerSubtype(ProfileUpdateStreamEvent.class, StreamEventType.PROFILE_UPDATE.getName())
            .registerSubtype(ProfileCreateStreamEvent.class, StreamEventType.PROFILE_CREATE.getName());

        gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapterFactory(typeAdapterFactory)
            .create();
    }

    public List<StreamEvent> parseJson(List<String> input) throws JsonParseException {
        return input.stream()
            .map(this::tryParsingStreamEvent)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    private StreamEvent tryParsingStreamEvent(final String streamEventJson) {
        try {
            return gson.fromJson(streamEventJson, StreamEvent.class);
        } catch (Exception e) {
            return null;
        }
    }
}
