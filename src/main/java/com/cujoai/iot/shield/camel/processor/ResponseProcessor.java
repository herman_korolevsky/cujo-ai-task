package com.cujoai.iot.shield.camel.processor;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.ResponseAction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ResponseProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseProcessor.class);
    private static final String JSON_PROPERTY_DEVICE_ID = "device_id";
    private static final String JSON_PROPERTY_REQUEST_ID = "request_id";
    private static final String JSON_PROPERTY_ACTION = "action";
    private final Gson gson;

    public ResponseProcessor() {
        gson = new GsonBuilder().create();
    }

    @Override
    public void process(final Exchange exchange) {
        final Message message = exchange.getMessage();
        final ResponseAction action = message.getHeader(Constants.HEADER_ACTION, ResponseAction.class);
        final RequestStreamEvent requestStreamEvent = message.getBody(RequestStreamEvent.class);
        final String deviceId = requestStreamEvent.getDeviceId();
        final String requestId = requestStreamEvent.getRequestId();
        final String body = ResponseAction.SET_DEVICE_TO_QUARANTINE.equals(action) ?
            createResponseJson(JSON_PROPERTY_DEVICE_ID, deviceId, action) :
            createResponseJson(JSON_PROPERTY_REQUEST_ID, requestId, action);
        message.setBody(body, String.class);

        LOGGER.info("Response message: {}", body);
    }

    private String createResponseJson(final String idProperty, final String deviceId, final ResponseAction action) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(idProperty, deviceId);
        jsonObject.addProperty(JSON_PROPERTY_ACTION, action.name());
        return StringUtils.appendIfMissing(gson.toJson(jsonObject), "\n");
    }
}
