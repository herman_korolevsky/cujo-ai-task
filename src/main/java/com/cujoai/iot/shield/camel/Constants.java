package com.cujoai.iot.shield.camel;

public final class Constants {

    public static final String INPUT_ROUTE = "seda:start";
    public static final String OUTPUT_FILE_ROUTE = "file://output/?autoCreate=true&fileExist=Append&fileName=output.json";
    public static final String REST_STATISTICS_ROUTE = "netty4-http:http://0.0.0.0:8080/statistics/?httpMethodRestrict=GET";

    public static final String HEADER_EVENT_TYPE = "event_type";
    public static final String HEADER_DELAY_MILLIS = "delay_millis";
    public static final String HEADER_ACTION = "action";

    public static final String AMOUNT_OF_PROTECTED_DEVICES = "amount of protected devices";
    public static final String AMOUNT_OF_SUSPECTED_DEVICES = "amount of suspected devices";
    public static final String AMOUNT_OF_MISSED_BLOCKS = "amount of missed blocks";
    public static final String AMOUNT_OF_INCORRECT_BLOCKS = "amount of incorrect blocks";

    private Constants() {
    }
}
