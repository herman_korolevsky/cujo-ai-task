package com.cujoai.iot.shield.camel.processor;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.PolicyType;
import com.cujoai.iot.shield.domain.Profile;
import com.cujoai.iot.shield.domain.ProfileCreateStreamEvent;
import com.cujoai.iot.shield.domain.ProfileUpdateStreamEvent;
import com.cujoai.iot.shield.domain.StreamEventType;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ProfileProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProfileProcessor.class);
    private final Map<String, Profile> profileStore;

    public ProfileProcessor(final Map<String, Profile> profileStore) {
        this.profileStore = profileStore;
    }

    @Override
    public void process(final Exchange exchange) {
        final Message message = exchange.getMessage();
        final StreamEventType type = message.getHeader(Constants.HEADER_EVENT_TYPE, StreamEventType.class);

        if (StreamEventType.PROFILE_CREATE.equals(type)) {
            createNewProfile(message.getBody(ProfileCreateStreamEvent.class));
        } else if (StreamEventType.PROFILE_UPDATE.equals(type)) {
            updateProfile(message.getBody(ProfileUpdateStreamEvent.class));
        } else {
            throw new IllegalArgumentException(String.format(
                Locale.getDefault(),
                "Stream event type %s is unsupported",
                type
            ));
        }
    }

    private void createNewProfile(final ProfileCreateStreamEvent createStreamEvent) {
        final Profile profile = createProfile(createStreamEvent);
        final String modelName = createStreamEvent.getModelName();
        profileStore.putIfAbsent(modelName, profile);
        LOGGER.info("New profile was created for {} with policy: {}, list of exceptions: {}",
            modelName,
            profile.getPolicyType(),
            profile.getExceptionsList());
    }

    private void updateProfile(final ProfileUpdateStreamEvent updateStreamEvent) {
        final String modelName = updateStreamEvent.getModelName();
        if (profileStore.containsKey(modelName)) {
            final PolicyType policy = profileStore.get(modelName).getPolicyType();
            List<String> newExceptionsList = PolicyType.BLOCK.equals(policy) ?
                updateStreamEvent.getWhitelist() :
                updateStreamEvent.getBlacklist();
            profileStore.put(modelName, new Profile(policy, newExceptionsList));
            LOGGER.info("Profile was updated for {} with policy: {}, list of exceptions: {}",
                modelName,
                policy,
                newExceptionsList);
        } else {
            LOGGER.warn("Profile does not exist, cannot update for model name {}",
                modelName);
        }

    }

    private Profile createProfile(final ProfileCreateStreamEvent event) {
        return new Profile(event.getDefaultPolicy(),
            PolicyType.BLOCK.equals(event.getDefaultPolicy()) ?
                event.getWhitelist() :
                event.getBlacklist()
        );
    }
}
