package com.cujoai.iot.shield.camel.route;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.camel.processor.ProfileProcessor;
import com.cujoai.iot.shield.domain.StreamEventType;
import java.util.Map;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ShieldRoute extends RouteBuilder {

    private final Processor profileProcessor;
    private final Processor requestProcessor;
    private final Processor responseProcessor;
    private final Map<String, Integer> statisticsStore;

    public ShieldRoute(ProfileProcessor profileProcessor,
        final Processor requestProcessor,
        final Processor responseProcessor,
        Map<String, Integer> statisticsStore) {
        this.profileProcessor = profileProcessor;
        this.requestProcessor = requestProcessor;
        this.responseProcessor = responseProcessor;
        this.statisticsStore = statisticsStore;
    }

    @Override
    public void configure() {
        from(Constants.INPUT_ROUTE)
            .delay(header(Constants.HEADER_DELAY_MILLIS).getExpression())
            .choice()
                .when(header(Constants.HEADER_EVENT_TYPE).isEqualTo(StreamEventType.REQUEST))
                    .process(requestProcessor)
                .otherwise()
                    .process(profileProcessor)
            .end()
            .choice()
                .when(header(Constants.HEADER_EVENT_TYPE).isEqualTo(StreamEventType.REQUEST))
                    .process(responseProcessor)
                    .to(Constants.OUTPUT_FILE_ROUTE)
            .end();

        from(Constants.REST_STATISTICS_ROUTE)
            .process(exchange -> exchange.getOut().setBody(statisticsStore.toString()));
    }
}
