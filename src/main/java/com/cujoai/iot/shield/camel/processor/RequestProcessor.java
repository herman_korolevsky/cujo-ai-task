package com.cujoai.iot.shield.camel.processor;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.PolicyType;
import com.cujoai.iot.shield.domain.Profile;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.ResponseAction;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class RequestProcessor implements Processor {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestProcessor.class);
    private final Map<String, Profile> profileStore;
    private final Map<String, Integer> statisticsStore;
    private final Set<String> deviceIds;
    private final Set<String> suspectedDeviceIds;
    private final Set<String> absentProfileModelNames;

    public RequestProcessor(final Map<String, Profile> profileStore, final Map<String, Integer> statisticsStore) {
        this.profileStore = profileStore;
        this.statisticsStore = statisticsStore;
        deviceIds = new HashSet<>();
        suspectedDeviceIds = new HashSet<>();
        absentProfileModelNames = new HashSet<>();
    }

    @Override
    public void process(final Exchange exchange) {
        final Message message = exchange.getMessage();
        final RequestStreamEvent requestStreamEvent = message.getBody(RequestStreamEvent.class);
        final String url = requestStreamEvent.getUrl();
        final String modelName = requestStreamEvent.getModelName();
        final String deviceId = requestStreamEvent.getDeviceId();

        LOGGER.info("Received request from {}, device id {} to access {}", modelName, deviceId, url);

        deviceIds.add(deviceId);
        statisticsStore.put(Constants.AMOUNT_OF_PROTECTED_DEVICES, deviceIds.size());

        final ResponseAction action = getResponseAction(url, modelName);

        if (ResponseAction.SET_DEVICE_TO_QUARANTINE.equals(action)) {
            suspectedDeviceIds.add(deviceId);
            statisticsStore.put(Constants.AMOUNT_OF_SUSPECTED_DEVICES, suspectedDeviceIds.size());
        }
        LOGGER.info("Request is {}", action);
        message.setHeader(Constants.HEADER_ACTION, action);
    }

    private ResponseAction getResponseAction(final String url, final String modelName) {
        final ResponseAction action;
        if (!profileStore.containsKey(modelName)) {
            action = ResponseAction.BLOCKED;
            absentProfileModelNames.add(modelName);
            statisticsStore.put(Constants.AMOUNT_OF_INCORRECT_BLOCKS, absentProfileModelNames.size());
        } else {
            final Profile profile = profileStore.get(modelName);
            final List<String> exceptionsList = profile.getExceptionsList();

            action = PolicyType.BLOCK.equals(profile.getPolicyType()) ?
                checkWhitelist(exceptionsList, url) :
                checkBlacklist(exceptionsList, url);
        }
        return action;
    }

    private ResponseAction checkBlacklist(final List<String> exceptionsList, final String url) {
        return exceptionsList.contains(url) ?
            ResponseAction.BLOCKED :
            ResponseAction.ALLOWED;
    }

    private ResponseAction checkWhitelist(final List<String> exceptionsList, final String url) {
        return exceptionsList.contains(url) ?
            ResponseAction.ALLOWED :
            ResponseAction.SET_DEVICE_TO_QUARANTINE;
    }
}
