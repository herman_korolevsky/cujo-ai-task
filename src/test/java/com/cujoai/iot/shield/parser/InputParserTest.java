package com.cujoai.iot.shield.parser;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.cujoai.iot.shield.domain.ProfileCreateStreamEvent;
import com.cujoai.iot.shield.domain.ProfileUpdateStreamEvent;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.StreamEvent;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class InputParserTest {

    @Test
    void parseLines() {
        final List<String> lines = new ArrayList<>() {{
            add("{\"type\": \"profile_create\", \"model_name\": \"iPhone\", \"default\": \"allow\", \"whitelist\": [], \"blacklist\": [\"fake_bank.com\"], \"timestamp\": 1563951285360}");
            add("{\"type\": \"profile_update\", \"model_name\": \"IOT Lightbulb\", \"whitelist\": [\"api.lightbulb.io\"], \"timestamp\": 1563951341680}");
            add("{\"type\": \"request\", \"request_id\": \"00ee3fde22f24384a21fbfcd2ba2ddc8\", \"model_name\": \"iPhone\", \"device_id\": \"d7c29fd60cee4e5f848bd198bcc2e100\", \"url\": \"bbc.co.uk\", \"timestamp\": 1563951288854}");
            add("{\"type\": \"profile_update\", \"model_name\": \"iPhone\", \"blacklist\": [1563951307799], \"timestamp\": \"bad_site.com\"}");
        }};

        InputParser parser = new InputParser();

        final List<StreamEvent> result = parser.parseJson(lines);

        assertEquals(3, result.size());
        assertFalse(result.contains(null));
        assertEquals(ProfileCreateStreamEvent.class, result.get(0).getClass());
        assertEquals(ProfileUpdateStreamEvent.class, result.get(1).getClass());
        assertEquals(RequestStreamEvent.class, result.get(2).getClass());
    }

}