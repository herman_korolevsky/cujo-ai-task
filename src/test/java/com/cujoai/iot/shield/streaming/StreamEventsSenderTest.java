package com.cujoai.iot.shield.streaming;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.StreamEvent;
import com.cujoai.iot.shield.domain.StreamEventType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.camel.ProducerTemplate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class StreamEventsSenderTest {

    private StreamEventsSender sender;
    @Mock
    ProducerTemplate template;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        sender = new StreamEventsSender(template);
    }

    @Test
    void testSentMessages() {
        final HashMap<String, Object> headersForFirst = new HashMap<>() {{
            put(Constants.HEADER_DELAY_MILLIS, 0);
            put(Constants.HEADER_EVENT_TYPE, StreamEventType.REQUEST);
        }};
        final HashMap<String, Object> headersForSecond = new HashMap<>() {{
            put(Constants.HEADER_DELAY_MILLIS, 198);
            put(Constants.HEADER_EVENT_TYPE, StreamEventType.REQUEST);
        }};

        final RequestStreamEvent first = createStreamEvent(123);
        final RequestStreamEvent second = createStreamEvent(321);
        final List<StreamEvent> streamEvents = new ArrayList<>() {{
            add(first);
            add(second);
        }};

        sender.sendEvents(streamEvents);

        verify(template, times(1)).sendBodyAndHeaders(eq(Constants.INPUT_ROUTE), eq(first), refEq(headersForFirst));
        verify(template, times(1)).sendBodyAndHeaders(eq(Constants.INPUT_ROUTE), eq(second), refEq(headersForSecond));
    }

    private RequestStreamEvent createStreamEvent(final int timestamp) {
        final RequestStreamEvent requestStreamEvent = new RequestStreamEvent();
        requestStreamEvent.setTimestamp(timestamp);
        return requestStreamEvent;
    }
}