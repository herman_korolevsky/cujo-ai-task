package com.cujoai.iot.shield.camel.processor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.PolicyType;
import com.cujoai.iot.shield.domain.Profile;
import com.cujoai.iot.shield.domain.ProfileCreateStreamEvent;
import com.cujoai.iot.shield.domain.ProfileUpdateStreamEvent;
import com.cujoai.iot.shield.domain.StreamEventType;
import java.util.Collections;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class ProfileProcessorTest {

    private Processor processor;

    @Mock
    Map<String, Profile> store;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        processor = new ProfileProcessor(store);
    }

    @Test
    void processCreateProfile() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);

        when(exchange.getMessage()).thenReturn(message);
        when(message.getHeader(Constants.HEADER_EVENT_TYPE, StreamEventType.class)).thenReturn(StreamEventType.PROFILE_CREATE);
        when(message.getBody(ProfileCreateStreamEvent.class)).thenReturn(createProfileCreateEvent());

        processor.process(exchange);

        verify(store, times(1)).putIfAbsent(eq("iphone"), refEq(new Profile(PolicyType.BLOCK, Collections.singletonList("good.site"))));
    }

    @Test
    void processUpdateProfile() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);

        when(exchange.getMessage()).thenReturn(message);
        when(message.getHeader(Constants.HEADER_EVENT_TYPE, StreamEventType.class)).thenReturn(StreamEventType.PROFILE_UPDATE);
        when(message.getBody(ProfileUpdateStreamEvent.class)).thenReturn(createProfileUpdateEvent());
        when(store.containsKey("iphone")).thenReturn(true);
        when(store.get("iphone")).thenReturn(createPolicy());

        processor.process(exchange);

        verify(store, times(1)).put(eq("iphone"), refEq(new Profile(PolicyType.BLOCK, Collections.singletonList("very.good.site"))));
    }

    @Test
    void processUpdateProfileWhichDoesNotExist() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);

        when(exchange.getMessage()).thenReturn(message);
        when(message.getHeader(Constants.HEADER_EVENT_TYPE, StreamEventType.class)).thenReturn(StreamEventType.PROFILE_UPDATE);
        when(message.getBody(ProfileUpdateStreamEvent.class)).thenReturn(createProfileUpdateEvent());
        when(store.containsKey("iphone")).thenReturn(false);

        processor.process(exchange);

        verify(store, never()).put(anyString(), any(Profile.class));
    }

    private ProfileCreateStreamEvent createProfileCreateEvent() {
        final ProfileCreateStreamEvent profileCreateStreamEvent = new ProfileCreateStreamEvent();
        profileCreateStreamEvent.setDefaultPolicy(PolicyType.BLOCK);
        profileCreateStreamEvent.setWhitelist(Collections.singletonList("good.site"));
        profileCreateStreamEvent.setModelName("iphone");
        profileCreateStreamEvent.setTimestamp(123);
        return profileCreateStreamEvent;
    }

    private ProfileUpdateStreamEvent createProfileUpdateEvent() {
        final ProfileUpdateStreamEvent profileUpdateStreamEvent = new ProfileUpdateStreamEvent();
        profileUpdateStreamEvent.setWhitelist(Collections.singletonList("very.good.site"));
        profileUpdateStreamEvent.setModelName("iphone");
        profileUpdateStreamEvent.setTimestamp(124);
        return profileUpdateStreamEvent;
    }

    private Profile createPolicy() {
        return new Profile(PolicyType.BLOCK, Collections.singletonList("good.site"));
    }
}