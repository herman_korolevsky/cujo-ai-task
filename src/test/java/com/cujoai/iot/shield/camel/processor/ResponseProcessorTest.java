package com.cujoai.iot.shield.camel.processor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.PolicyType;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.ResponseAction;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ResponseProcessorTest {

    private Processor processor;

    @BeforeEach
    void setup() {
        processor = new ResponseProcessor();
    }

    @Test
    void quarantineMessageResponse() throws Exception {
        final String expected = "{\"device_id\":\"123\",\"action\":\"SET_DEVICE_TO_QUARANTINE\"}\n";
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("bad.site"));
        when(message.getHeader(Constants.HEADER_ACTION, ResponseAction.class)).thenReturn(ResponseAction.SET_DEVICE_TO_QUARANTINE);

        processor.process(exchange);

        verify(message, times(1)).setBody(expected, String.class);
    }

    @Test
    void regularMessageResponse() throws Exception {
        final String expected = "{\"request_id\":\"456\",\"action\":\"BLOCKED\"}\n";
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("bad.site"));
        when(message.getHeader(Constants.HEADER_ACTION, ResponseAction.class)).thenReturn(ResponseAction.BLOCKED);

        processor.process(exchange);

        verify(message, times(1)).setBody(expected, String.class);
    }

    private RequestStreamEvent createRequestEvent(final String url) {
        final RequestStreamEvent requestStreamEvent = new RequestStreamEvent();
        requestStreamEvent.setDeviceId("123");
        requestStreamEvent.setUrl(url);
        requestStreamEvent.setRequestId("456");
        requestStreamEvent.setModelName("iphone");
        requestStreamEvent.setTimestamp(789);
        return requestStreamEvent;
    }
}