package com.cujoai.iot.shield.camel.processor;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.cujoai.iot.shield.camel.Constants;
import com.cujoai.iot.shield.domain.PolicyType;
import com.cujoai.iot.shield.domain.Profile;
import com.cujoai.iot.shield.domain.RequestStreamEvent;
import com.cujoai.iot.shield.domain.ResponseAction;
import java.util.Collections;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class RequestProcessorTest {

    private Processor processor;

    @Mock
    Map<String, Profile> profileMap;
    @Mock
    Map<String, Integer> statisticsMap;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        processor = new RequestProcessor(profileMap, statisticsMap);
    }

    @Test
    void processRequestBlockedAction() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("bad.site"));
        when(profileMap.containsKey("iphone")).thenReturn(true);
        when(profileMap.get("iphone")).thenReturn(createPolicy(PolicyType.ALLOW, "bad.site"));

        processor.process(exchange);

        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_PROTECTED_DEVICES, 1);
        verify(message, times(1)).setHeader(Constants.HEADER_ACTION, ResponseAction.BLOCKED);
    }

    @Test
    void processRequestBlockedActionNoProfile() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("bad.site"));
        when(profileMap.containsKey("iphone")).thenReturn(false);

        processor.process(exchange);

        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_PROTECTED_DEVICES, 1);
        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_INCORRECT_BLOCKS, 1);
        verify(message, times(1)).setHeader(Constants.HEADER_ACTION, ResponseAction.BLOCKED);
    }

    @Test
    void processRequestAllowedActionBlockingPolicy() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("good.site"));
        when(profileMap.containsKey("iphone")).thenReturn(true);
        when(profileMap.get("iphone")).thenReturn(createPolicy(PolicyType.BLOCK, "good.site"));

        processor.process(exchange);

        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_PROTECTED_DEVICES, 1);
        verify(message, times(1)).setHeader(Constants.HEADER_ACTION, ResponseAction.ALLOWED);
    }

    @Test
    void processRequestAllowedActionAllowingPolicy() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("good.site"));
        when(profileMap.containsKey("iphone")).thenReturn(true);
        when(profileMap.get("iphone")).thenReturn(createPolicy(PolicyType.ALLOW, "bad.site"));

        processor.process(exchange);

        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_PROTECTED_DEVICES, 1);
        verify(message, times(1)).setHeader(Constants.HEADER_ACTION, ResponseAction.ALLOWED);
    }

    @Test
    void processRequestQuarantineAction() throws Exception {
        final Exchange exchange = mock(Exchange.class);
        final Message message = mock(Message.class);
        when(exchange.getMessage()).thenReturn(message);
        when(message.getBody(RequestStreamEvent.class)).thenReturn(createRequestEvent("bad.site"));
        when(profileMap.containsKey("iphone")).thenReturn(true);
        when(profileMap.get("iphone")).thenReturn(createPolicy(PolicyType.BLOCK, "good.site"));

        processor.process(exchange);

        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_PROTECTED_DEVICES, 1);
        verify(statisticsMap, times(1)).put(Constants.AMOUNT_OF_SUSPECTED_DEVICES, 1);
        verify(message, times(1)).setHeader(Constants.HEADER_ACTION, ResponseAction.SET_DEVICE_TO_QUARANTINE);
    }

    private RequestStreamEvent createRequestEvent(final String url) {
        final RequestStreamEvent requestStreamEvent = new RequestStreamEvent();
        requestStreamEvent.setDeviceId("123");
        requestStreamEvent.setUrl(url);
        requestStreamEvent.setRequestId("456");
        requestStreamEvent.setModelName("iphone");
        requestStreamEvent.setTimestamp(789);
        return requestStreamEvent;
    }

    private Profile createPolicy(final PolicyType policy, final String exception) {
        return new Profile(policy, Collections.singletonList(exception));
    }
}